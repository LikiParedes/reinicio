## Página web de Reinicio, servicio de reparación de computadoras.

© Reinicio, 2021. 

Salvo que se indique lo contrario, todo el *contenido publicado* en la página web de *Reinicio* se encuentra bajo los términos de la **Licencia Creative Commons Atribución-NoComercial-SinDerivadas 4.0 Internacional**.

Consulte la página de [Creative Commons](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.es) para más detalles.

El *código fuente* de la página web se resguarda bajo lo estipulado por la **Licencia Pública General GNU**.

Este archivo forma parte del código fuente de la página web de Reinicio.

Los archivos de código de la página web de *Reinicio* son software libre: pueden redistribuirse y/o modificarse bajo los términos de la Licencia Pública General GNU publicada por la Free Software Foundation, ya sea la versión 3 o cualquier versión posterior.

El código fuente de la página web de Reinicio se distribuye con la esperanza de que sea útil pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o ADECUACIÓN A UN PROPÓSITO PARTICULAR.

Consulte la página de [GNU](https://www.gnu.org/licenses/gpl-3.0.html) para más detalles.


